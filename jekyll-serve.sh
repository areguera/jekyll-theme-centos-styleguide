#!/usr/bin/env bash

test -d .bundle || mkdir .bundle
test -d .bundle && podman unshare chown -R ${USER} .bundle

podman run --rm --volume="$PWD:/srv/jekyll:z" --volume=".bundle/:/usr/local/bundle:z" -p 4000:4000/tcp -it jekyll/jekyll jekyll serve
