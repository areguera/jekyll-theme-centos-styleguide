#!/usr/bin/env bash

echo -n "Enter title: "
read title

echo -n "Enter image url: "
read image

echo -n "Enter category [blog]: "
read category

filename=_posts/$(date +%F)-$(echo "$title" | sed -r -e 's/[[:punct:]]/-/g' -e 's/[[:space:]]+/-/g' -e 's/-+/-/g' -e 's,-$,,').md

test -f $filename && echo "ERROR: ${filename} already exists!" && exit 1

cat << EOF > "$filename"
---
layout: post
title: ${title}
date: $(date +"%Y-%m-%d %H:%M:%S %z")
image: ${image}
category: ${category:-blog}
---

EOF

${EDITOR} ${filename}
