---
title: jekyll-theme-centos
title_lead: 0.1.25
layout: aside
---

## Overview

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos)
mission is to satisfy the [CentOS Project](https://www.centos.org/) content
presentation necessities, in open, simple and beautiful way.

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) provides
ready-to-use assets, includes and layouts to make easier the edition, search
and management dynamic contents like release downloads (with different
versions, architectures, release notes, cloud images, etc.), links to
download mirrors (with dynamic search box), news, events, sponsors and more
static information like project mission and organization.

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses the
following jekyll plugins:

* [jekyll-feeds](https://github.com/jekyll/jekyll-feed) -- v0.15.1
* [jekyll-toc](https://github.com/toshimaru/jekyll-toc) -- v0.15.0
* [jekyll-pagination-v2](https://github.com/sverrirs/jekyll-paginate-v2) -- v3.0.0

When you are [editing CentOS Project
website](https://git.centos.org/centos/centos.org) you can customize even
further the ready-to-use assets, includes and layouts that
[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) provides
by copying them into the site and changing them there.  To know more about
jekyll theme customization, see [Jekyll
Themes](https://jekyllrb.com/docs/themes/).

## Installation

The installation process is described in [Jekyll
Installation](https://jekyllrb.com/docs/installation/) page. This section
describes how to run jekyll commands using [podman](https://podman.io/):

1. Install the `podman` command:

   ```shell
   ~] sudo dnf -y install podman
   ```

   See other installation methods at <https://podman.io/getting-started/installation>.

2. Clone
   [jekyll-theme-centos-styleguide](https://gitlab.com/areguera/jekyll-theme-centos-styleguide.git)
   repository:

   ```shell
   ~]$ git clone git@gitlab.com:areguera/jekyll-theme-centos-styleguide.git ~/Projects/MySite
   ```

   This repository provides a ready-to-use Jekyll instance configured to use
   [jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) and
   its dependencies. So, you can concentrate on producing content without any
   sort configuration distraction.

5. Change your working directory to recently cloned repository:

   ```shell
   ~]$ cd ~/Projects/MySite
   ```

6. Run `jekyll-serve.sh` script:

   ```shell
   MySite]$ ./jekyll-serve.sh
   ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux-musl]
   Configuration file: /srv/jekyll/_config.yml
               Source: /srv/jekyll
          Destination: /srv/jekyll/_site
    Incremental build: disabled. Enable with --incremental
         Generating... 
          Jekyll Feed: Generating feed for posts
            AutoPages: Disabled/Not configured in site.config.
           Pagination: Complete, processed 1 pagination page(s)
                       done in 0.739 seconds.
    Auto-regeneration: enabled for '/srv/jekyll'
       Server address: http://0.0.0.0:4000//
     Server running... press ctrl-c to stop.
   ```

   The first time you run this script it will connect to <https://rubygems.org>
   and download all requirements and dependencies defined in the `Gemfile`.
   This download process may take a while and only occurs once.  The files
   downloaded are stored locally in a directory named `.bundle` to avoid
   permission issues. 

5. Use your web browser to access the location `http://localhost:4000/`.

## Configuration

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses
configuration variables in the `_config.yml` file to allow you customization of
site branding, header, navigation links and footer. Feel free to change the
value of these configuration variables to fit your needs. This section
describes each configuration variable one by one.

### Branding

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) provides
a design where the brand information is always visible on the left-top corner
of the site. It follows a monolithic visual structure where there is one unique
name (logo) reinforced by an artistic motif (header). The logo doesn't change
but the artistic motif background image does with each major release of CentOS
distribution to establish a strong visual connection between visual
manifestations attached to the same visual structure.  The branding
configuration variables control the value of both logo a artistic motif.

When you are creating a site for the CentOS Project you don't have to change
the value of any branding configuration variable since they already provide the
CentOS Project branding information.  However, if your are creating a site for
a project other than CentOS Project, you must change the values of these
variables to fit the visual identity of your project.  Please, read about
[CentOS Trademarks](https://www.centos.org/legal/trademarks/) first.

The branding configuration variables are:

* `identity.logo.image`

  Control the image used as logo always visible on the left-top corner of the
  site. This image is restricted to 32 pixels height and must fit well on black
  background to reach higher contrast possible.

  Default value: `/assets/img/logo.png`.

* `identity.logo.link`

  Control the link set to `identity.logo.image`.

  Default value: `/`

* `identity.motif.image`

  Control the image used as background on all site headers. This is a 1200x530
  px PNG image that must allow white text on top of it to read very well. The
  purpose of this image is to reinforce the visual connection within the site
  and the site with other visual manifestations that share the same style.

  Default value: `/assets/img/motif.png`

### Navigation bar

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) provides
a design where the navigation bar is always visible on the top-right corner of
the site.  The navigation bar (navbar) configuration variables control the
presence of links and menu of links in it. It is also source of value for
other configuration variables (see footer).

* `navbar[{name: {icon: "", link: "", menu: [{name: "", link: ""}]}}]`

  Default value:

  ```yaml
  navbar:
    - name: "About"
      icon: "fas fa-info-circle"
      link: "#"
      menu:
        - name: "About CentOS"
          link: "/about"
        - name: "Frequently Asked Questions (FAQs)"
          link: "https://wiki.centos.org/FAQ"
        - name: "Special Interest Groups (SIGs)"
          link: "https://wiki.centos.org/SpecialInterestGroups"
        - name: "CentOS Variants"
          link: "/variants"
        - name: "Governance"
          link: "http://localhost:4000/about/governance"
    - name: "Community"
      icon: "fas fa-users"
      link: "#"
      menu:
        - name: "Contribute"
          link: "https://wiki.centos.org/Contribute"
        - name: "Forums"
          link: "https://www.centos.org/forums/"
        - name: "Mailing Lists"
          link: "https://wiki.centos.org/GettingHelp/ListInfo"
        - name: "IRC"
          link: "https://wiki.centos.org/irc"
        - name: "Calendar &amp; IRC Meeting List"
          link: "/community/calendar/"
        - name: "Planet"
          link: "http://planet.centos.org/"
        - name: "Submit Bug"
          link: "https://bugs.centos.org/"
    - name: "Search"
      icon: "fas fa-search"
      link: "/search"
      menu: []
  ```

### Header

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) provides
the `home` layout with a space between the navigation bar and the page body to
highlight the main worklines your project is focused on, along an introductory
preamble.  The header configuration variables allow you to customize this
information.

The header configuration variables are:

* `header.preamble`

  Control the worklines introductory message. The `home` layout uses this
  variable in the header section after the site description as introduction to
  worklines section.

  Default value: Undefined

* `header.worklines[{icon: "", name: "", link: ""}]`

  Control the workline buttons visible in the `home` layout. A workline button
  highlight the area of your project that demands effort and is the reason of
  its existence.

  Default value: Undefined

### Footer

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) provides
a footer with dynamic columns to put links from `navbar`, one static column for
information about the site (ej., title, description, social media links), 
the copyright sentence with legal links on the right beneath them all. The
footer configuration variables allow you to customize these values.

* `footer{columns: [], social: {icon: "", link: ""}, copyright: {author: "", legals: [{text: "", link: ""}]}}`

  Default value:

  ```yaml
  footer:
    columns:
      - "About"
      - "Community"
    social:
      - icon: "fab fa-facebook-f"
        link: "https://www.facebook.com/groups/centosproject/"
      - icon: "fab fa-twitter"
        link: "https://twitter.com/centosproject"
      - icon: "fab fa-youtube"
        link: "https://youtube.com/TheCentOSProject"
      - icon: "fab fa-linkedin"
        link: "https://www.linkedin.com/groups/22405"
      - icon: "fab fa-reddit"
        link: "https://www.reddit.com/r/CentOS/"
    copyright:
      author: "The CentOS Project"
      legals:
        - text: "Legal"
          link: "/legal"
        - text: "Privacy"
          link: "/privacy"
        - text: "Site source"
          link: "https://git.centos.org/centos/centos.org"
  ```

## Assets

Assets are described in [Jekyll Assets](https://jekyllrb.com/docs/assets/)
page. This section describes how
[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses
assets directories.

### The `/assets/css/` directory

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses the
file `/assets/css/` to store the `centos.bootstrap.min.scss` file. This file
imports the `scss` definition from `_sass` directory. Normally, it only has the
import line and no other line. The empty front matter is a Jekyll requirements.

```
---
---

@import "centos/centos";
```

### The `_sass` directory

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses the
`_sass` directory to store `scss` files. The information in this directory is
compiled by Jekyll and stored as minimized CSS file in
`_/site/assets/css/centos.bootstrap.min.css`, the file loaded on HTML files
published on-line.

```
_sass
├── bootstrap
├── centos
└── fontawesome

3 directories, 0 files
```

Both `_sass/bootstrap` and `_sass/fontawesome` directories contain the `scss`
files related to Bootstrap and Font Awesome projects, respectively. They were
imported directly from these projects' source code using `npm` command.
[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos)
developers don't make changes in these two directories but in `_sass/centos`
directory instead.

```
_sass/centos
├── fonts
├── mixins
├── centos.scss
├── _datatable.scss
├── _fonts.scss
├── _footer.scss
├── _header.scss
├── _highlighter.scss
├── _mixins.scss
├── _nav.scss
├── _page.scss
├── _rssfeeds.scss
└── _variables.scss

2 directories, 11 files
```

### The `/assets/img/` directory

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses the
following images to control the site visual identity:

`/assets/img/logo.png` -- The project's logo (symbol + typography). It is a
32 pixel height PNG image always visible in the navigation bar on top. Must be
designed to contrast with the navigation bar background color.

{% include page/figure.html src="/assets/img/logo.png" alt="Logo" %}

`/assets/img/favicon.png` -- The project's symbol. It is a 16x16 pixels PNG
image visible in web browser tabs and bookmarks.

{% include page/figure.html src="/assets/img/favicon.png" alt="Logo" %}

`/assets/img/motif.png` -- The project's artistic motif. It is a 1200x530
pixels PNG image visible behind the titles on all pages. Its purpose is to
strength the visual connection between site pages and other visual
manifestations of the project that use the same artistic motif.

{% include page/figure.html src="/assets/img/motif.png" alt="Logo" %}

This image must expose the [artistic
motif](https://wiki.centos.org/ArtWork/Motifs) published in last major release
of CentOS distribution currently available. The image must be under a dark tone
to allow white text to read well on top of it.

### The `/assets/fonts/` directory

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses
`Montserrat` webfonts from [The Montserrat Font
Project](https://github.com/JulietaUla/Montserrat) to text in page content and
titles while [Font-Awesome-Free](https://github.com/FortAwesome/Font-Awesome)
on icons.

### The `/assets/js/` directory

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) is built
on top of the following components:

* [jQuery](https://www.npmjs.com/package/jquery) -- v3.5.1
* [Bootstrap](https://www.npmjs.com/package/bootstrap) -- v4.5.3
* [Font Awesome Free](https://www.npmjs.com/package/@fortawesome/fontawesome-free) -- v5.15.1
* [DataTables](https://datatables.net/) -- v1.10.22
* [Lunrjs](https://jekyllcodex.org/without-plugin/search-lunr/) -- v2.3.9
* [moment.js](https://www.npmjs.com/package/moment) -- v2.29.1
* [jquery-rss](https://www.npmjs.com/package/jquery-rss) -- v4.3.0

## Layouts

Layouts are described in [Jekyll Layouts](https://jekyllrb.com/docs/layouts/)
page. This section describes the ready-to-use layouts that jekyll-theme-centos
provides, when to use them, and how to do it. 

### The `default` layout

The `default` layout is the base layout other layout files stand on. It
provides the most basic HTML structure a page could have but not structure to
page content.  Normally, the website doesn't set the `default` layout directly
in the front matter section of pages. However, if you need to, it is possible
to test the `default` on pages by setting the following front matter
definition:

```markdown
---
layout: default
title: Default layout
title_lead: Example
---

Page paragraphs here ...
```

[<i class="fas fa-eye"></i> Example](/example-layout-default)

### The `home` layout

The `home` layout controls the presentation of `index.md`, the first page one
see when access the site address using a web browser. It includes sections
"News and events", "Sponsors", and posts "Around CentOS".  The sections "News
and Events" and "Sponsor" are configured as
[collections](https://jekyllrb.com/docs/collections/). This allows us to
retrieve content from page files stored in the `_posts` and `_sponsors`
directories, respectively. 

* See [Manage news and events collection](#news-and-events).
* See [Manage sponsors collection](#sponsors).

The section "Around CentOS" uses `jquery-rss.js` to load posts from
[planet.rss](https://www.centos.org/assets/planet.rss) file online.

The `home` layout definition should be set in `index.md` page using the
following definition:

```markdown
---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: The CentOS Project
title_lead: |
  Community-driven free software effort focused on delivering a robust open
  source ecosystem around a Linux platform.
---
```

[<i class="fas fa-eye"></i> Example](/example-layout-home)

### The `page` layout

The `page` layout offers one-column design holding the navigation breadcrumbs,
table of content, and page content vertically aligned.  Use the `page` layout
when you consider the content you want to present is so extensive
that the `aside` layout is too narrow for it.

To create content based on one-column layout, use the following definition:

```markdown
---
layout: page
title: One Column Layout
title_lead: Example
permalink: /:path/:basename/index.html
---

...
```

[<i class="fas fa-eye"></i> Example](/example-layout-page)

### The `aside` layout

The `aside` layout offers a two-columns design and is the preferred layout we
use. The left column holds the navigation breadcrumbs and table of content
while the right column the page content. The left column is stuck when the page
content is scrolls down to grantee permanent visibility to navigation links.
Additionally, when there are too many sections in the page content, the left
side section shows a scroll bar to allow access to them without scrolling down
the page content.

To create content based on `aside` layout, use the following definition:

```markdown
---
layout: aside
title: Two-Columns Layout
title_lead: Example
permalink: /:path/:basename/index.html
---

...
```

[<i class="fas fa-eye"></i> Example](/example-layout-aside)

### The `blog` layout

The `blog` layout offers the index of posts stored in `_posts` directory. This
layout is configured to show 10 posts on a single page. When there are more
than 10 posts, the `blog` layout is configured to show pagination to older and
newer posts.

To create content based on `blog` layout, use the following front matter
definition:

```markdown
---
layout: blog
title: Blog
title_lead: News, views, and reports on CentOS.
pagination: 
  enabled: true
---
```

[<i class="fas fa-eye"></i> Example](/blog)

### The `post` layout

The `post` layout offers the detailed view of posts, it is a two-columns design
similar to `aside` layout but with the following differences:

* Navigation breadcrumbs are stuck to `{% raw %}Home / {{ page.category }} / {{ page.title
}}{% endraw %}`. The navigation breadcrumbs in the `aside` can reflect several
nesting levels of content and is conceived for pages not posts. In posts, the
navigation breadcrumbs reflects a single level of content, the page title of
the post.

* Optional image on top of content.

* Navigation between older and newer immediate posts.

To create content based on `post` layout, use the following front matter
definition:

```markdown
---
layout: post
title: Welcome to Jekyll!
title_lead: Example using post layout
date: 2020-12-18 08:28:43 -0600
image: https://jekyllrb.com/img/jekyll-og.png
category: "blog"
---
```

[<i class="fas fa-eye"></i> Example](/blog/2020/12/19/Hello-World-1.html)

The `category` attribute associates the post to the page where posts are
indexed. In the context of `jekyll-theme-centos-styleguide`, the `category`
attribute must be always set to `blog`, the name of page that provides the
index of posts with pagination.  Otherwise, in case a different category value
be provided, the breadcrumb link will point to a page where no index or
pagination exist (unless you've prepared it first, of course).

The `image` attribute is optional. When it is present, it shouldn't be empty
and must provide a URL, local or remote, pointing an image.

### The `search` layout

The `search` layout allows your site to have a simple search functionality to
look for content on pages and posts.

```markdown
---
title: Search
title_lead: Look for content in this website.
layout: search
---

{% raw %}{% include page/search.html %}{% endraw %}
```

### The `download` layout

The `download` layout is responsible of gearing the presentation of
[release download](#manage-release-download) information (e.g., versioning,
architecture, packages), [release
documentation](#manage-release-documentation) and [release
end-of-life](#manage-release-end-of-life) for each distribution the CentOS
project provides. 

```markdown
---
title: Download
layout: download
permalink: /:path/:basename/index.html
---

...
```

The design itself is result of combining several html files under
`_includes/page/` directory that have been created to describe the project's
present reality around the distributions it provides. When this reality changes,
the combination of these include files must also change to describe it.
Changes in the `_includes/page` directory structure can take place in the site
directory structure or in the jekyll-theme-centos directory structure. Being
the directory structure of jekyll-theme-centos the preferred location when you
want all site instances created from it to get the actualization after a gem
update.

The information presented is not under `_includes/page` directory structure but
under `_data/` directory structure.  See, how to:

* [Manage release download](#manage-release-download)
* [Manage release documentation](#manage-release-documentation)
* [Manage release end-of-life](#manage-release-end-of-life)

### The `download-mirror` layout

```markdown
---
title: Mirror List
title_lead: List of CentOS official mirrors.
layout: download-mirror
permalink: /:path/:basename/index.html
---

{% raw %}CentOS welcomes new mirror sites.  If you are considering setting up a public
mirror site for CentOS, [please follow the mirror
guidelines](http://wiki.centos.org/HowTos/CreatePublicMirrors) to make sure
that your mirror is consistent with the other mirror sites.  If you're
searching for mirrors providing AltArch content (like ppc64, ppc64le, aarch64,
armfhp) please use [this link](/download/mirrors-altarch/).

<table id="download-mirror">
<thead>
  <tr>
    <th class="col-0">Location</th>
    <th class="col-0">Region</th>
    <th class="col-12">Sponsor</th>
    <th class="col-0">HTTP</th>
    <th class="col-0">HTTPS</th>
    <th class="col-0">Rsync</th>
  </tr>
</thead>
<tbody>
{% for row in site.data.full-mirrorlist %}
  <tr>
    <td class="text-nowrap">{{ row["Location"] }}</td>
    <td class="text-nowrap">{{ row["Region"] }}</td>
    <td>{% if row["Sponsor URL"] != "" %}<a href="{{ row["Sponsor URL"] }}">{{ row["Sponsor"] }}</a>{% else %}{{ row["Sponsor"] }}{% endif %}</td>
    <td class="text-nowrap">{% if row["HTTP mirror"] != "" %}<a href="{{ row["HTTP mirror"] }}"><i class="fas fa-server"></i> Mirror</a>{% endif %}</td>
    <td class="text-nowrap">{% if row["HTTPS mirror"] != "" %}<a href="{{ row["HTTPS mirror"] }}"><i class="fas fa-server"></i> Mirror</a>{% endif %}</td>
    <td class="text-nowrap">{% if row["Rsync link"] != "" %}<a href="{{ row["Rsync link"] }}"><i class="fas fa-link"></i> Link</a>{% endif %}</td>
  </tr>
{% endfor %}
</tbody>
<tfoot>
  <tr>
    <th class=""></th>
    <th class=""></th>
    <th class=""></th>
    <th class=""></th>
    <th class=""></th>
    <th class=""></th>
  </tr>
</tfoot>
</table>{% endraw %}
```

## Posts

The posts feature is described in [Jekyll
Posts](https://jekyllrb.com/docs/posts/) page. This section describes how
[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses
posts to organize content.

1. Create a post `_posts/<YEAR>-<MONTH>-<DAY>-<title>.md` and be sure it uses
   the front matter described in [the `post` layout](#the-post-layout). You can
   do this manually or running `jekyll-post.sh` script:

   ```shell
   ./jekyll-post.sh
   ```

   When you run the `jekyll-post.sh` script it asks you for the front matter
   information the `post` layout needs, creates the file under `_posts`
   directory in case it doesn't exist and launches your text editor to write
   the post content. In case the file under `_posts` already exists, the script
   ends with an error and tells you about it. To name this file,
   `jekyll-post.sh` uses Jekyll convention `YEAR-MONTH-DAY-title.MARKUP`.

2. Content in `_posts/<YEAR>-<MONTH>-<DAY>-<title>.md` is normally written once
   and not edited once published. In case you need to extend the information
   create a new post or a page for it.

3. Posts are automatically linked from the [/blog](/blog) page.

You shouldn't create directory levels unless you modify the default blog
structure
[jekyll-theme-centos-styleguide](https://gitlab.com/areguera/jekyll-theme-centos-styleguide.git)
provides to recognize it.

## Pages

The page feature is described in [Jekyll
Pages](https://jekyllrb.com/docs/pages/) page. This section describes how
[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses
pages to organize content.

1. Create a page `<page>.md` and be sure it uses the front matter described in
   [the `page` layout](#the-page-layout).

2. When content in `<page>.md` starts to grow you can fragment it creating a
   directory `<page>` and new topic pages inside it in the form
   `<page>/<topic>.md`.

3. Link content from `<page>.md` to `<page>/<topic>.md`.

You can create as many directory levels as necessary.

## Collections

### Manage news and events

In the `home` layout, the "News and events" section shows the title and excerpt
(first paragraph) of the last 3 posts created with `category` name `blog`.
Using this category name is necessary to keep consistent links between the
`post` layout and the `blog` layout. Otherwise, links in the navigation
breadcrumbs of `post` layout may point to an indexing page that doesn't exist.

```markdown
---
title: This is an entry for news and events
date:  "2020-12-19 16:30:50 -0300"
category: blog
---

Paragraph describing the entry in the home layout.

Paragraph extending the entry information.  
```

Using the post feature to create new entries under news and events section
reduces the need of changing HTML code directly.

### Manage sponsors

The sponsor page is rendered by files under the `_sponsors` directory. These
files must have the following format:

```markdown
---
name: artmotion
country: eu
logo: /assets/img/sponsors/am-logo.png
address: http://www.artmotion.eu
status: active
---
```

Images (ej., `/assets/img/sponsors/am-logo.png`) must exist in the specified location in order to be showed.

## Data files

[jekyll-theme-centos](https://gitlab.com/areguera/jekyll-theme-centos) uses
[Jekyll Data Files](https://jekyllrb.com/docs/datafiles/) to sustainably manage
presentation of information related to CentOS releases, downloads, mirrors, and
elements that may change based on external controlled conditions (e.g., new
mirrors are added or removed).

### Manage release download

The information related to release downloads is stored in a CSV file at
`_data/centos-<distro>/<version>.csv` file. For example, in the
case of CentOS Stream 8, the file we use would be the following:

```
_data/centos-stream-8.csv
```

This file must start with the following line:

```csv
arch,iso,rpm,cloud,containers,vagrant
```

According with the first line fields, it must continue with the content:

```csv
x86_64,http://isoredirect.centos.org/centos/8-stream/isos/x86_64/,http://mirror.centos.org/centos/8-stream/,-,-,-
ARM64 (aarch64),http://isoredirect.centos.org/centos/8-stream/isos/aarch64/,http://mirror.centos.org/centos/8-stream/,-,-,-
IBM Power (ppc64le),http://isoredirect.centos.org/centos/8-stream/isos/ppc64le/,http://mirror.centos.org/centos/8-stream/,-,-,-
```

### Manage release documentation

The information related to release documentation is stored in a CSV file at
`_data/centos-<distro>-<version>-doc.csv` file. For example, in the case
of CentOS Stream 8, the file we use would be the following:

```
_data/centos-stream-8-doc.csv
```

This file must start with the following line:

```csv
release_notes,release_email,documentation
```

According with the first line fields, it must continue with the content:

```csv
-,-,-
```

### Manage release end-of-life

The information related to release documentation is stored in a CSV file at
`_data/centos-<distro>-<version>-eol.csv` file. For example, in the case
of CentOS Stream 8, the file we use would be the following:

```
_data/centos-stream-8-eol.csv
```

This file must start with the following line:

```csv
date,url
```

According with the first line fields, it must continue with the content:

```csv
N/A,-
```

The second field (url) is optional here, and when set it is presented using the
round information symbol next to the date. It was introduced to allow comments
like the one related to eol reduction in case of CentOS Linux 8.

### Manage download mirrors

The presentation of data related to download mirrors is controlled by the
`download-mirrors` layout and the following CSV files:

* `_data/full-mirrorlist.csv`
* `_data/altarch-mirrorlist.csv`

These files must start with the following line:

```csv
"Location","Region","Sponsor","Sponsor URL","HTTP mirror","HTTPS mirror","Rsync link"
```

According with the first line fields, it must continue with the content:

```csv
"Africa","Botswana","Retention Range (PTY) Ltd","http://www.retentionrange.co.bw/","http://mirror.retentionrange.co.bw/centOS/","",""
```
