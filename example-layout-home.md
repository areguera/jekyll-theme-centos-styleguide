---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: The CentOS Project
title_lead: |
  Community-driven free software effort focused on delivering a robust open
  source ecosystem around a Linux platform.
---
